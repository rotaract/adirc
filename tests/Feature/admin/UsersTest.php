<?php

namespace Tests\Feature\admin;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_admins_can_manage_users()
    {
        $this->get('/admin/users')->assertRedirect('/login');

        $this->actingAs(factory('App\User')->create());

        $this->get('/admin/users')->assertStatus(403);
    }

    /** @test */
    public function it_list_all_users()
    {
        $this->actingAs(factory('App\User')->state('admin')->create());
        factory('App\Subscription', 2)->create();

        $this->get('/admin/users')
            ->assertViewIs('admin.users.index');
    }
}
