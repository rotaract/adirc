<?php

namespace Tests\Feature\admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_admins_can_manage_users_payments()
    {
        $subscription = factory('App\Subscription')->create();
        $this->actingAs(factory('App\User')->create());

        $this->get("/admin/users/{$subscription->user->id}")->assertStatus(403);
        $this->get("/admin/users/{$subscription->user->id}/payment")->assertStatus(403);
        $this->post("/admin/users/{$subscription->user->id}/payment")->assertStatus(403);
    }

    /** @test */
    public function it_validates_the_payment_fields()
    {
        $this->actingAs(factory('App\User')->state('admin')->create());
        $subscription = factory('App\Subscription')->create();

        $this->post("/admin/users/{$subscription->user->id}/payment")
            ->assertSessionHasErrors(['payment_type', 'value', 'ticket_batch']);

        $this->post("/admin/users/{$subscription->user->id}/payment", [
            'payment_type' => 'bank_deposit',
            'ticket_batch' => 2,
            'value' => 'aslkfasl'
        ])->assertSessionHasErrors(['value', 'payment_type']);
    }

    /** @test */
    public function it_updates_the_user_payment()
    {
        $this->actingAs(factory('App\User')->state('admin')->create());
        $subscription = factory('App\Subscription')->create();

        $this->post("/admin/users/{$subscription->user->id}/payment", [
            'payment_type' => '2',
            'ticket_batch' => '3',
            'value' => 120
        ])->assertRedirect("/admin/users/{$subscription->user->id}");

        $subscription = $subscription->fresh();

        $this->assertEquals(2, $subscription->payment_type);
        $this->assertEquals(3, $subscription->ticket_batch);
        $this->assertEquals(120, $subscription->value);
        $this->assertEquals(\App\Subscription::PAID, $subscription->status);
    }

    // it must sends an email to the user
}
