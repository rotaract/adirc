<?php

namespace Tests\Feature\register;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_register_itself()
    {
        $this->withoutExceptionHandling();

        $club = factory('App\Club')->create();
        $user = [
            'name' => 'John Doe',
            'email' => 'some@email.com',
            'password' => 'samepassword',
            'password_confirmation' => 'samepassword',
            'nickname' => 'Palmas',
            'birth_date' => '1990-04-23',
            'phone' => '(42) 12345-7894',
            'club_id' => $club->id,
            'address' => 'Rua dos bobos, numero 0',
            'city' => 'palmas',
            'blood_type' => 'O+',
            'emergency_contact_name' => 'My Father',
            'emergency_contact_phone' => '42 123456789',
            'allergy' => 'bees',
            'food_restriction' => 'milk',
            'rg' => '12345678',
            'cpf' => '76206831434',
            'agreed' => true,
        ];

        $this->post('/register', $user)->assertRedirect('/home');
        $registered_user = User::latest()->first();

        $this->assertAuthenticated();
        $this->assertDatabaseHas('users', array_except(array_merge($user, ['type' => 0]), ['password', 'password_confirmation']));
        $this->assertDatabaseHas('subscriptions', [
            'user_id' => $registered_user->id,
            'status' => \App\Subscription::NOT_PAID,
            'ticket_batch' => null,
            'value' => null,
            'tax' => null,
            'payment_type' => null,
        ]);
    }

    /** @test */
    public function it_validates_the_required_fields()
    {
        $this->post('/register')
            ->assertSessionHasErrors(['name', 'email', 'password', 'birth_date', 'phone', 'club_id', 'address', 'city', 'blood_type', 'emergency_contact_name', 'emergency_contact_phone', 'rg', 'cpf', 'agreed']);
    }

    /** @test */
    public function it_checks_the_birth_date()
    {
        $this->post('/register', ['birth_date' => '23/04/1990'])
            ->assertSessionHasErrors(['birth_date']);

        // checking if the user is at least 18yrs old
        $this->post('/register', ['birth_date' => '2001-04-14'])
            ->assertSessionHasErrors(['birth_date']);
    }

    /** @test */
    public function it_validates_the_cpf()
    {
        $this->post('/register', ['cpf' => '12345678'])
            ->assertSessionHasErrors(['cpf']);
    }

    /** @test */
    public function it_validates_the_terms_of_service()
    {
        $this->post('/register', ['agreed' => false])
            ->assertSessionHasErrors(['agreed']);
    }

    /** @test */
    public function it_checks_if_a_valid_club_was_entered()
    {
        $this->post('/register', ['club_id' => 'guarapuava'])
            ->assertSessionHasErrors(['club_id']);

        $this->post('/register', ['club_id' => 10])
            ->assertSessionHasErrors(['club_id']);
    }
}
