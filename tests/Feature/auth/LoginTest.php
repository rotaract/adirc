<?php

namespace Tests\Feature\auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_login()
    {
        $user = factory('App\User')->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response->assertRedirect("/home");
        $this->assertAuthenticatedAs($user);
    }

    /** @test */
    public function the_form_validates_required_fields()
    {
        $response = $this->post('/login');

        $response->assertSessionHasErrors(['email', 'password']);
    }

    /** @test */
    public function a_user_cannot_login_with_invalid_credentials()
    {
        $this->post('/login', [
            'email' => 'some@email.com',
            'password' => 'invalid password',
        ])->assertSessionHasErrors('email');
    }

    /** @test */
    public function an_admin_can_login()
    {
        $this->withoutExceptionHandling();

        $user = factory('App\User')->states('admin')->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response->assertRedirect("/admin/users");
        $this->assertAuthenticatedAs($user);
    }
}
