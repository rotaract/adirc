<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_lists_all_users()
    {
        $this->withoutExceptionHandling();
        factory(User::class, 10)->create();

        $this->get('/list')
            ->assertViewIs('users');
    }
}
