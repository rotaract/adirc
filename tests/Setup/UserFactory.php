<?php

namespace Tests\Setup;

use App\Subscription;
use App\User;

class UserFactory
{
    private $subscriptionInfo = [];

    public function create($user = [], $howMany = 1)
    {
        $users = factory(User::class, $howMany)->create($user);

        foreach($users as $user) {
            factory(Subscription::class)->create($this->subscriptionInfo + [
                'user_id' => $user->id
            ]);
        }

        return $users;
    }

    public function withSubscription($params = [])
    {
        $this->subscriptionInfo = $params;

        return $this;
    }
}
