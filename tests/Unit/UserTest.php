<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_hashes_the_password_attribute_automatically()
    {
        $user = factory('App\User')->create(['password' => 'secret']);

        $this->assertNotEquals('secret', $user->password);
        $this->assertTrue(Hash::check('secret', $user->password));
    }

    /** @test */
    public function it_checks_if_the_user_is_an_admin()
    {
        $user = factory('App\User')->state('admin')->create();

        $this->assertTrue($user->isAdmin());
    }
}
