<?php

namespace Tests\Unit;

use App\Club;
use App\Subscription;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Setup\UserFactory;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_confirms_a_subscription_paid_using_credit_card()
    {
        $subscription = factory(Subscription::class)->create();

        $subscription->confirm(Subscription::PAYMENT_CREDIT_CARD, 105, 5.11, 2);
        $subscription->fresh();

        $this->assertEquals($subscription->status, Subscription::PAID);
        $this->assertEquals($subscription->payment_type, Subscription::PAYMENT_CREDIT_CARD);
        $this->assertEquals($subscription->value, 105);
        $this->assertEquals($subscription->tax, 5.11);
        $this->assertEquals($subscription->ticket_batch, 2);
    }

    /** @test */
    public function it_scopes_subscriptions_by_paid()
    {
        factory(Subscription::class, 5)->create(['status' => Subscription::NOT_PAID]);
        factory(Subscription::class, 3)->create(['status' => Subscription::PAID]);

        $this->assertEquals(3, Subscription::paid()->count());
    }

    /** @test */
    public function it_groups_paid_subscriptions_by_ticket_batch()
    {
        factory(Subscription::class, 5)->create(['status' => Subscription::PAID, 'ticket_batch' => 1]);
        factory(Subscription::class, 8)->create(['status' => Subscription::PAID, 'ticket_batch' => 2]);
        factory(Subscription::class, 2)->create(['status' => Subscription::PAID, 'ticket_batch' => 3]);
        factory(Subscription::class, 2)->create(['status' => Subscription::NOT_PAID, 'ticket_batch' => 1]);

        $by_batch = Subscription::paidByBatch();

        $this->assertEquals(5, $by_batch[1]->total);
        $this->assertEquals(8, $by_batch[2]->total);
        $this->assertEquals(2, $by_batch[3]->total);
    }
}
