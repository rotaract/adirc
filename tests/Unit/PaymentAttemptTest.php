<?php

namespace Tests\Unit;

use App\PaymentAttempt;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentAttemptTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_calculates_the_price_with_tax()
    {
        $attempt = factory('App\PaymentAttempt')->create([
            'value' => 95,
            'tax' => 4.99
        ]);

        $this->assertEquals($attempt->priceWithTax(), 99.99);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $attempt = factory('App\PaymentAttempt')->create();

        $this->assertInstanceOf('App\User', $attempt->user);
    }

    /** @test */
    public function if_an_attempt_for_the_current_batch_does_not_exists_create_a_new_one()
    {
        $user = factory('App\User')->create();

        $attempt = PaymentAttempt::findForUser($user);

        $this->assertDatabaseHas('payment_attempts', [
            'user_id' => $user->id,
            'ticket_batch' => config('payment.current_batch'),
            'value' => config('payment.current_batch_price'),
            'tax' => config('payment.current_batch_tax'),
        ]);
        $this->assertNotNull($attempt->ref);
    }

    /** @test */
    public function if_an_attempt_for_the_current_batch_alreay_exists_dont_create_other()
    {
        $user = factory('App\User')->create();
        $attempt = factory('App\PaymentAttempt')->create([
            'user_id' => $user->id
        ]);

        $attempt = PaymentAttempt::findForUser($user);
        $this->assertCount(1, PaymentAttempt::where('user_id', $user->id)->get());
    }

    /** @test */
    public function if_an_attempt_for_other_batch_exists_create_a_new_one()
    {
        $user = factory('App\User')->create();
        $attempt = factory('App\PaymentAttempt')->create([
            'user_id' => $user->id,
            'ticket_batch' => 2,
            'value' => 10,
            'tax' => 1,
        ]);

        $attempt = PaymentAttempt::findForUser($user);
        $this->assertCount(2, PaymentAttempt::where('user_id', $user->id)->get());
    }

    /** @test */
    public function if_the_status_is_in_process_dont_override_external_ref()
    {
        $user = factory('App\User')->create();
        $attempt = factory('App\PaymentAttempt')->create([
            'user_id' => $user->id,
            'status' => 'in_process',
            'ref' => 'test'
        ]);


        $this->assertCount(1, PaymentAttempt::where('user_id', $user->id)->get());
        $this->assertEquals('test', PaymentAttempt::findForUser($user)->ref);
    }

    /** @test */
    public function if_the_status_is_approved_dont_override_external_ref()
    {
        $user = factory('App\User')->create();
        $attempt = factory('App\PaymentAttempt')->create([
            'user_id' => $user->id,
            'status' => 'approved',
            'ref' => 'test'
        ]);


        $this->assertCount(1, PaymentAttempt::where('user_id', $user->id)->get());
        $this->assertEquals('test', PaymentAttempt::findForUser($user)->ref);
    }
}
