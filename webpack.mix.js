const mix = require('laravel-mix');
var tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/css/app.sass', 'public/css').options({
      processCssUrls: false,
      postCss: [ tailwindcss('./resources/css/tailwind.js'), ]
   })
   .sourceMaps();

 mix.copyDirectory('resources/images', 'public/images');
 mix.copyDirectory('resources/downloads', 'public/downloads');

if (mix.inProduction()) {
    mix.version();
}
