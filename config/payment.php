<?php

return [
    'client_id' => env('MERCADOPAGO_CLIENT_ID'),
    'client_secret' => env('MERCADOPAGO_CLIENT_SECRET'),

    'current_batch' => env('TICKET_BATCH_NUMBER', 3),
    'current_batch_name' => env('TICKET_BATCH_NAME', 'ADIRC Mistérios do Egito - 3º Lote'),
    'current_batch_price' => env('TICKET_BATCH_PRICE', 120),
    'current_batch_tax' => env('TICKET_BATCH_TAX', 4.99),
];
