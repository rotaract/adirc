<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/list', 'PagesController@userList');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/payment', 'PaymentController@index')->middleware('auth');

Route::post('/admin/hooks/payment', 'PaymentController@update');

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::redirect('/admin', '/admin/users');
    Route::get('/admin/users', 'Admin\UsersController@index');
    Route::get('/admin/users/{user}', 'Admin\UsersController@show');
    Route::get('/admin/users/{user}/payment', 'Admin\UsersController@payment');
    Route::post('/admin/users/{user}/payment', 'Admin\UsersController@updatePayment');

    Route::get('/admin/statistics', 'Admin\StatisticsController@general');
    Route::get('/admin/name-list', 'Admin\ReportsController@names');
    Route::get('/admin/allergy-list', 'Admin\ReportsController@allergies');
    Route::get('/admin/food-restrictions-list', 'Admin\ReportsController@foodRestrictions');
});
