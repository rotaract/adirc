<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('nickname')->nullable();
            $table->date('birth_date');
            $table->string('phone');
            $table->integer('club_id')->unsigned();
            $table->string('address');
            $table->string('city');
            $table->boolean('is_guest');
            $table->string('blood_type');
            $table->string('emergency_contact_name');
            $table->string('emergency_contact_phone');
            $table->string('allergy')->nullable();
            $table->string('food_restriction')->nullable();
            $table->string('rg');
            $table->string('cpf');
            $table->boolean('agreed');
            $table->integer('type')->unsigned()->default(0);

            $table->rememberToken();
            $table->timestamps();

            $table->foreign('club_id')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
