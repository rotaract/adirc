<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => 'Admin User',
            'email' => 'admin@test.com',
            'type' => App\User::TYPE_ADMIN,
        ]);
    }
}
