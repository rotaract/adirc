<?php

use Illuminate\Database\Seeder;

class ClubTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubs = [
            'Palmas',
            'Guarapuava',
            'Pato Branco',
        ];

        collect($clubs)->map(function ($item, $key) {
            \App\Club::create(['name' => $item]);
        });
    }
}
