<?php

use App\Subscription;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'type' => App\User::TYPE_MEMBER,
        'email_verified_at' => now(),
        'password' => 'secret',
        'remember_token' => str_random(10),
        'nickname' => $faker->name,
        'birth_date' => $faker->date('Y-m-d', '2000-01-01'),
        'phone' => $faker->e164PhoneNumber,
        'club_id' => function () {
            return factory('App\Club')->create()->id;
        },
        'address' => $faker->address,
        'city' => $faker->city,
        'is_guest' => false,
        'blood_type' => 'AB-',
        'emergency_contact_name' => $faker->name,
        'emergency_contact_phone' => $faker->e164PhoneNumber,
        'allergy' => null,
        'rg' => $faker->rg,
        'cpf' => $faker->cpf,
        'agreed' => true,
    ];
});

$factory->state(App\User::class, 'admin', function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'type' => 1,
        'email_verified_at' => now(),
        'password' => 'secret',
        'remember_token' => str_random(10),
        'nickname' => $faker->name,
        'birth_date' => $faker->date('Y-m-d', '2000-01-01'),
        'phone' => $faker->e164PhoneNumber,
        'club_id' => function () {
            return factory('App\Club')->create()->id;
        },
        'address' => $faker->address,
        'city' => $faker->city,
        'is_guest' => false,
        'blood_type' => 'AB-',
        'emergency_contact_name' => $faker->name,
        'emergency_contact_phone' => $faker->e164PhoneNumber,
        'allergy' => null,
        'rg' => $faker->rg,
        'cpf' => $faker->cpf,
        'agreed' => true,
    ];
});
