<?php

use Faker\Generator as Faker;

$factory->define(App\PaymentAttempt::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'ticket_batch' => 1,
        'value' => 95,
        'tax' => 4.99,
    ];
});
