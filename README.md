This is a simple project to manage subscriptions for the XXIX ADIRC.

## Features

- Allow users subscriptions
- Manage payments (credit card or bank deposit)
- Reports to support the event (people with allergies, food restriction, special needs, etc)
- Backup using [https://github.com/spatie/laravel-backup](https://github.com/spatie/laravel-backup) and Dropbox

## Setup

- clone the project: `git clone git@gitlab.com:rotaract/adirc.git`
- copy env file: `cp .env.example .env`
- migrate database: `php artisan migrate && php artisan db:seed`
- boot up the server: `php artisan serve`

## Testing

You can run the test suite using phpunit: `vendor/bin/phpunit` 

