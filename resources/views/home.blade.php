@extends('layouts.app')

@section('content')
<div class="container py-4">
  <h1 class="font-normal text-4xl">Bem vindo(a), {{ auth()->user()->name }}!</h1>

  <p class="mt-6 leading-normal">
    É uma honra contar com sua presença para a <strong>XXIX ADIRC</strong>, estamos ansiosos para receber você e sua caravana.
  </p>

  <p class="mt-2 leading-normal">
    Faltam <span class="text-2xl font-bold">{{ $remaining_days }}</span> dias para o evento
  </p>
</div>
@endsection
