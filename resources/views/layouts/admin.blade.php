<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="bg-grey-lightest">
    <div>
        <nav class="bg-blue p-6 no-print">
          <div class="container mx-auto flex justify-between items-center">
            <a href="/admin/users" class="text-white no-underline font-semibold text-xl tracking-tight mr-8">
              {{ config('app.name', 'Laravel') }}
            </a>

            <div class="flex-1 -mx-4">
              <a href="/admin/users" class="text-white no-underline mx-4">Inscrições</a>
              <a href="/admin/statistics" class="text-white no-underline mx-4">Estatísticas</a>
              <a href="/admin/name-list" class="text-white no-underline mx-4">Lista de Inscritos</a>
              <a href="/admin/allergy-list" class="text-white no-underline mx-4">Alergias</a>
              <a href="/admin/food-restrictions-list" class="text-white no-underline mx-4">Restrição alimentar</a>
            </div>

            <div>
              <a href="/logout" class="text-white no-underline">Sair</a>
            </div>
          </div>

          <div class="block hidden">
            <button class="flex items-center px-3 py-2 border rounded text-blue-lighter border-blue-light hover:text-white hover:border-white">
              <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
            </button>
          </div>
        </nav>

        <main class="mt-4 container mx-auto">
          @yield('content')
        </main>
    </div>
</body>
</html>
