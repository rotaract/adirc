@extends('layouts.admin')

@section('content')

  <h1 class="font-thin text-3xl mb-4">Lista de Inscritos</h1>

  <div class="w-full p-4 bg-white">
    <table class="w-full">
      <thead class="text-left text-2xl">
        <tr>
          <th class="py-2">Nome</th>
          <th>Crachá</th>
          <th>Clube</th>
        </tr>
      </thead>

      <tbody>
        @foreach($users as $user)
        <tr>
          <td class="py-3"><a href="/admin/users/{{ $user->id }}" class="link">{{ $user->name }}</a></td>
          <td>{{ $user->nickname }}</td>
          <td>{{ $user->club->name }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
