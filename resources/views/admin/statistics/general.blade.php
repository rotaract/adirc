@extends('layouts.admin')

@section('content')

<h1 class="font-thin text-3xl mb-4">Estatísticas</h1>

<div class="flex justify-center pb-4 border-b">
  <div class="bg-white p-4 w-1/4 text-center rounded shadow h-48 flex flex-col justify-center">
    <p class="text-grey-darker font-sm mb-4">Total de Inscritos Confirmados</p>
    <h2 class="font-thin text-5xl">{{ $total }}</h2>
  </div>
</div>

<h2 class="font-thin text-3xl my-4 text-center w-full">Total por lotes</h2>

<div class="flex justify-center my-6 -mx-4 pb-4 border-b">
  <div class="mx-4 bg-white p-4 w-1/4 text-center rounded shadow h-48 flex flex-col justify-center">
    <p class="text-grey-darker font-sm mb-4">1º Lote</p>
    <h2 class="font-thin text-5xl">{{ isset($by_batch[1]) ? $by_batch[1]->total : 0 }}</h2>
  </div>

  <div class="mx-4 bg-white p-4 w-1/4 text-center rounded shadow h-48 flex flex-col justify-center">
    <p class="text-grey-darker font-sm mb-4">2º Lote</p>
    <h2 class="font-thin text-5xl">{{ isset($by_batch[2]) ? $by_batch[2]->total : 0 }}</h2>
  </div>

  <div class="mx-4 bg-white p-4 w-1/4 text-center rounded shadow h-48 flex flex-col justify-center">
    <p class="text-grey-darker font-sm mb-4">3º Lote</p>
    <h2 class="font-thin text-5xl">{{ isset($by_batch[3]) ? $by_batch[3]->total : 0 }}</h2>
  </div>

  <div class="mx-4 bg-white p-4 w-1/4 text-center rounded shadow h-48 flex flex-col justify-center">
    <p class="text-grey-darker font-sm mb-4">4º Lote</p>
    <h2 class="font-thin text-5xl">{{ isset($by_batch[4]) ? $by_batch[4]->total : 0 }}</h2>
  </div>
</div>

<h2 class="font-thin text-3xl my-4 text-center w-full">Total por clubes</h2>

<div class="flex justify-between flex-wrap my-6 -mx-4">
  @foreach ($clubs as $club)
    <div class="mx-4 my-4 bg-white p-4 w-1/4 flex-no-shrink text-center rounded shadow h-48 flex flex-col justify-center">
      <p class="text-grey-darker font-sm mb-4">{{ $club->name }}</p>
      <h2 class="font-thin text-5xl">
        {{ $club->users->where('subscription.status', 1)->count() }}
      </h2>
    </div>
  @endforeach
</div>

@endsection
