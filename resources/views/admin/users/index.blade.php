@extends('layouts.admin')

@section('content')
  <h1 class="font-thin text-4xl pb-4 border-b">Inscritos</h1>

  <div class="mt-4 w-1/2">
    <form action="">
      <div class="form-field">
        <label for="name" class="label">Nome</label>

        <input type="text" class="text-field" name="name" id="name" value="{{ request()->name }}" />
      </div>

      <div class="flex w-full">
        <div class="form-field mr-4 w-1/2">
          <label for="club_id" class="label">Clube</label>

          <div class="select-container w-full">
            <select class="select w-full" name="club_id" id="club_id">
              <option value="">Todos</option>
              @foreach ($clubs as $club)
                <option value="{{ $club->id }}" {{ request()->club_id == $club->id ? 'selected' : '' }}>{{ $club->name }}</option>
              @endforeach
            </select>
            <div class="select-chevron"></div>
          </div>
        </div>

        <div class="form-field w-1/2">
          <label for="status" class="label">Status da inscrição</label>

          <div class="select-container w-full">
            <select class="select w-full" name="status" id="status">
              <option value="">Todos</option>
              <option value="1" {{ request()->status == '1' ? 'selected' : '' }}>Confirmadas</option>
              <option value="0" {{ request()->status == '0' ? 'selected' : '' }}>Não confirmadas</option>
            </select>
            <div class="select-chevron"></div>
          </div>
        </div>
      </div>

      <div class="form-field text-right">
        <a href="/admin/users" class="no-underline text-grey-dark mr-2">Limpar</a>
        <button class="btn-primary">Filtrar</button>
      </div>
    </form>
  </div>

  <div class="mt-4 border-t pt-4">
    <table class="w-full text-left">
      <thead class="bg-blue-darker text-white">
        <tr>
          <th class="p-2">Nome</th>
          <th class="p-2">E-mail</th>
          <th class="p-2">Clube</th>
          <th class="p-2">Status da Inscrição</th>
        </tr>
      </thead>

      <tbody class="bg-white">
        @foreach ($users as $user)
          <tr class="hover:bg-blue-lightest">
            <td class="border-b px-2 py-4">
              <a href="{{ "/admin/users/{$user->id}" }}" class="link">{{ $user->name }}</a>
            </td>
            <td class="border-b px-2 py-4">{{ $user->email }}</td>
            <td class="border-b px-2 py-4">{{ $user->club->name }}</td>
            <td class="border-b px-2 py-4 {{ $user->subscription->status ? 'text-green-dark' : 'text-red-dark' }}">{{ App\Subscription::TEXT_STATUS[$user->subscription->status] }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  {{ $users->appends([
      'name' => request()->name,
      'club_id' => request()->club_id,
      'status' => request()->status,
    ])->links()
  }}
@endsection
