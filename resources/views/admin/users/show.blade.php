@extends('layouts.admin')

@section('content')
  <div class="flex items-end justify-between mb-4">
    <div class="flex items-end">
      <h1 class="font-thin text-4xl mr-4">{{ $user->name }}</h1>
      <span class="text-2xl text-grey-darker">({{ $user->nickname }})</span>
    </div>

    <div>
      @if(!$user->subscription->status)
        <a class="link mr-4" href="{{ "/admin/users/{$user->id}/payment" }}">Confirmar pagamento</a>
      @endif
      <a class="link" href="{{ "/admin/users" }}">Voltar</a>
    </div>
  </div>

  <div class="mb-4 text-xl {{ $user->subscription->status ? 'text-green-dark' : 'text-red' }}">
    {{ $user->is_guest ? 'Convidado' : 'Associado' }} -
    Inscrição {{ App\Subscription::TEXT_STATUS[$user->subscription->status] }}
  </div>

  <div class="bg-white p-4">
    <table>
      <tr>
        <td class="pr-6 py-2 text-right">Nome do crachá</td>
        <td class="font-bold">{{ $user->nickname }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Clube</td>
        <td class="font-bold">{{ $user->club->name }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Data de Nascimento</td>
        <td class="font-bold">{{ $user->birth_date->format('d/m/Y') }} <span class="text-grey-darker">({{ $user->birth_date->diffInYears() }} anos)</span></td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">RG</td>
        <td class="font-bold">{{ $user->rg }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">CPF</td>
        <td class="font-bold">{{ $user->cpf }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Telefone:</td>
        <td class="font-bold">{{ $user->phone }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Endereço:</td>
        <td class="font-bold">{{ $user->address }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Cidade:</td>
        <td class="font-bold">{{ $user->city }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Telefone:</td>
        <td class="font-bold">{{ $user->phone }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Tipo Sanguíneo:</td>
        <td class="font-bold">{{ $user->blood_type }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Em caso de emergência:</td>
        <td class="font-bold">{{ $user->emergency_contact_phone }} ({{ $user->emergency_contact_name }})</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Possui alergias?</td>
        <td class="font-bold">{{ $user->allergy }}</td>
      </tr>

      <tr>
        <td class="pr-6 py-2 text-right">Restrição alimentar?</td>
        <td class="font-bold">{{ $user->food_restriction }}</td>
      </tr>
    </table>
  </div>

  @if($user->subscription->status)
    <h2 class="my-4 font-thin">Dados do pagamento</h2>

    <div class="bg-white p-4">
      <table>
        <tr>
          <td class="pr-6 py-2 text-right">Lote</td>
          <td class="font-bold">{{ $user->subscription->ticket_batch }}º Lote</td>
        </tr>

        <tr>
          <td class="pr-6 py-2 text-right">Forma de pagamento</td>
          <td class="font-bold">{{ $user->subscription->paymentTypeText() }}</td>
        </tr>

        <tr>
          <td class="pr-6 py-2 text-right">Valor</td>
          <td class="font-bold">R$ {{ $user->subscription->value }}</td>
        </tr>
      </table>
    </div>
  @endif

@endsection
