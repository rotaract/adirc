@extends('layouts.admin')

@section('content')
<div class="flex items-end justify-between mb-4">
  <div class="flex items-end">
    <h1 class="font-thin text-4xl mr-4">{{ $user->name }}</h1>
    <span class="text-2xl text-grey-darker">({{ $user->nickname }})</span>
  </div>

  <div>
    <a class="link" href="{{ "/admin/users/{$user->id}" }}">Voltar</a>
  </div>
</div>

<div class="mt-8">
  <form action="{{ "/admin/users/{$user->id}" }}/payment" method="post" class="w-1/2">
    @csrf

    <div class="form-field {{ $errors->has('payment_type') ? 'has-error' : '' }}">
      <label for="payment_type" class="label">Forma de Pagamento</label>

      <div class="select-container w-full">
        <select name="payment_type" id="payment_type" class="select" id="payment_type">
          <option value="">(Selecione uma forma de pagamento)</option>
          <option value="2" {{ old('payment_type', $user->subscription->payment_type) == '2' ? 'selected' : '' }}>Depósito/Transferência</option>
          <option value="3" {{ old('payment_type', $user->subscription->payment_type) == '3' ? 'selected' : '' }}>Dinheiro</option>
          <option value="1" {{ old('payment_type', $user->subscription->payment_type) == '1' ? 'selected' : '' }}>Cartão de Crédito</option>
        </select>
        <div class="select-chevron"></div>
      </div>

      @if ($errors->has('payment_type'))
        <p class="form-hint">{{ $errors->first('payment_type') }}</p>
      @endif
    </div>

    <div class="form-field {{ $errors->has('ticket_batch') ? 'has-error' : '' }}">
      <label for="ticket_batch" class="label">Lote</label>

      <div class="select-container w-full">
        <select name="ticket_batch" id="ticket_batch" class="select">
          <option value="">(Selecione um lote)</option>
          <option value="1" {{ old('ticket_batch', $user->subscription->ticket_batch) == '1' ? 'selected' : '' }}>1º Lote</option>
          <option value="2" {{ old('ticket_batch', $user->subscription->ticket_batch) == '2' ? 'selected' : '' }}>2º Lote</option>
          <option value="3" {{ old('ticket_batch', $user->subscription->ticket_batch) == '3' ? 'selected' : '' }}>3º Lote</option>
          <option value="4" {{ old('ticket_batch', $user->subscription->ticket_batch) == '4' ? 'selected' : '' }}>4º Lote</option>
        </select>
        <div class="select-chevron"></div>
      </div>

      @if ($errors->has('ticket_batch'))
        <p class="form-hint">{{ $errors->first('ticket_batch') }}</p>
      @endif
    </div>

    <div class="form-field {{ $errors->has('value') ? 'has-error' : '' }}">
      <label for="value" class="label">Valor</label>

      <input type="text" class="text-field" name="value" value="{{ old('value', $user->subscription->value) }}"/>

      @if ($errors->has('value'))
        <p class="form-hint">{{ $errors->first('value') }}</p>
      @endif
    </div>

    <div class="form-field text-right">
      <button type="submit" class="btn-primary">Atualizar pagamento</button>
    </div>
  </form>
</div>
@endsection
