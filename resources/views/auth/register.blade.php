@extends('layouts.login')

@section('content')
<div class="container mt-8 text-black">
  <h1 class="pb-8 text-center text-4xl font-thin border-b-2">Cadastre-se e descubra os mistérios do Egito</h1>

  <div class="px-4 py-8">
    @if ($errors->any())
    <div class="bg-red-lightest p-8 border border-red text-red text-center mb-8 rounded">
      Encontramos alguns erros nos dados informados. Por favor, revise as informações e tente novamente.
    </div>
    @endif

    <form method="POST" action="{{ route('register') }}">
      @csrf

      <div class="flex flex-col md:flex-row pb-4 border-b-2">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-4">Informações pessoais</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name" class="label">Seu nome completo</label>
            <input type="text" class="text-field" id="name" name="name" value="{{ old('name') }}" autofocus />
            @if ($errors->has('name'))
              <p class="form-hint">{{ $errors->first('name') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('nickname') ? 'has-error' : '' }}">
            <label for="nickname" class="label">Nome que deseja no crachá</label>
            <input type="text" class="text-field" id="nickname" name="nickname" value="{{ old('nickname') }}" />
            @if ($errors->has('nickname'))
              <p class="form-hint">{{ $errors->first('nickname') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('birth_date') ? 'has-error' : '' }}">
            <label for="birth_date" class="label">Data de Nascimento</label>
            <input type="date" class="text-field" id="birth_date" name="birth_date" value="{{ old('birth_date') }}" />
            @if ($errors->has('birth_date'))
              <p class="form-hint">{{ $errors->first('birth_date') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('rg') ? 'has-error' : '' }}">
            <label for="rg" class="label">Seu RG</label>
            <input type="text" class="text-field" id="rg" name="rg" value="{{ old('rg') }}" />
            @if ($errors->has('rg'))
              <p class="form-hint">{{ $errors->first('rg') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('cpf') ? 'has-error' : '' }}">
            <label for="cpf" class="label">Seu CPF</label>
            <input type="text" class="text-field" id="cpf" name="cpf" value="{{ old('cpf') }}" />
            @if ($errors->has('cpf'))
              <p class="form-hint">{{ $errors->first('cpf') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="flex flex-col md:flex-row py-4 border-b-2">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-2">Contato</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('phone') ? 'has-error' : '' }}">
            <label for="phone" class="label">Telefone para contato</label>
            <input type="text" class="text-field" id="phone" name="phone" value="{{ old('phone') }}" />
            @if ($errors->has('phone'))
              <p class="form-hint">{{ $errors->first('phone') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('address') ? 'has-error' : '' }}">
            <label for="address" class="label">Endereço residencial</label>
            <input type="text" class="text-field" id="address" name="address" value="{{ old('address') }}" />
            @if ($errors->has('address'))
              <p class="form-hint">{{ $errors->first('address') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('city') ? 'has-error' : '' }}">
            <label for="city" class="label">Cidade em que mora</label>
            <input type="text" class="text-field" id="city" name="city" value="{{ old('city') }}" />
            @if ($errors->has('city'))
              <p class="form-hint">{{ $errors->first('city') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="flex flex-col md:flex-row py-4 border-b-2">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-2">Em caso de emergências</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('blood_type') ? 'has-error' : '' }}">
            <label for="blood_type" class="label">Tipo sanguíneo</label>
            <div class="select-container">
              <select name="blood_type" id="blood_type" class="select">
                <option value="">(Selecione um tipo sanguíneo)</option>
                <option value="O+" {{ old('blood_type') == 'O+' ? 'selected' : '' }}>O+</option>
                <option value="O-" {{ old('blood_type') == 'O-' ? 'selected' : '' }}>O-</option>
                <option value="A+" {{ old('blood_type') == 'A+' ? 'selected' : '' }}>A+</option>
                <option value="A-" {{ old('blood_type') == 'A-' ? 'selected' : '' }}>A-</option>
                <option value="AB+" {{ old('blood_type') == 'AB+' ? 'selected' : '' }}>AB+</option>
                <option value="AB-" {{ old('blood_type') == 'AB-' ? 'selected' : '' }}>AB-</option>
                <option value="B+" {{ old('blood_type') == 'B+' ? 'selected' : '' }}>B+</option>
                <option value="B-" {{ old('blood_type') == 'B-' ? 'selected' : '' }}>B-</option>
              </select>
              <div class="select-chevron"></div>
            </div>
            @if ($errors->has('blood_type'))
              <p class="form-hint">{{ $errors->first('blood_type') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('food_restriction') ? 'has-error' : '' }}">
            <label for="food_restriction" class="label">Possui alguma restrição alimentar?</label>
            <input type="text" class="text-field" id="food_restriction" name="food_restriction" value="{{ old('food_restriction') }}" />
            @if ($errors->has('food_restriction'))
              <p class="form-hint">{{ $errors->first('food_restriction') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('allergy') ? 'has-error' : '' }}">
            <label for="allergy" class="label">Possui algum tipo de alergia?</label>
            <input type="text" class="text-field" id="allergy" name="allergy" value="{{ old('allergy') }}" />
            @if ($errors->has('allergy'))
              <p class="form-hint">{{ $errors->first('allergy') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('emergency_contact_name') ? 'has-error' : '' }}">
            <label for="emergency_contact_name" class="label">Nome de um contato para emergências</label>
            <input type="text" class="text-field" id="emergency_contact_name" name="emergency_contact_name" value="{{ old('emergency_contact_name') }}" />
            @if ($errors->has('emergency_contact_name'))
              <p class="form-hint">{{ $errors->first('emergency_contact_name') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('emergency_contact_phone') ? 'has-error' : '' }}">
            <label for="emergency_contact_phone" class="label">Telefone para emergências</label>
            <input type="text" class="text-field" id="emergency_contact_phone" name="emergency_contact_phone" value="{{ old('emergency_contact_phone') }}" />
            @if ($errors->has('emergency_contact_phone'))
              <p class="form-hint">{{ $errors->first('emergency_contact_phone') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="flex flex-col md:flex-row py-4 border-b-2">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-2">Clube</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('club_id') ? 'has-error' : '' }}">
            <label for="club_id" class="label">Clube do qual faz parte</label>

            <div class="select-container">
              <select class="select" name="club_id" id="club_id">
                <option>(Selecione um clube)</option>
                @foreach ($clubs as $club)
                  <option value="{{ $club->id }}" {{ old('club_id') == $club->id ? 'selected' : '' }}>{{ $club->name }}</option>
                @endforeach
              </select>
              <div class="select-chevron"></div>
            </div>
            @if ($errors->has('club_id'))
              <p class="form-hint">{{ $errors->first('club_id') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('is_guest') ? 'has-error' : '' }}">
            <div class="checkbox-container">
              <input type="checkbox" value="true" id="is_guest" name="is_guest" {{ (bool)old('is_guest') ? 'checked' : '' }} />
              <label for="is_guest" class="label">Sou um convidado</label>
            </div>
            @if ($errors->has('is_guest'))
              <p class="form-hint">{{ $errors->first('is_guest') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="flex flex-col md:flex-row py-4 border-b-2">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-2">Informações de acesso</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('email') ? 'has-error' : '' }}">
            <label for="email" class="label">E-mail</label>
            <input type="text" class="text-field" id="email" name="email" value="{{ old('email') }}" />
            @if ($errors->has('email'))
              <p class="form-hint">{{ $errors->first('email') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('password') ? 'has-error' : '' }}">
            <label for="password" class="label">Informe uma senha para acesso</label>
            <input type="password" class="text-field" id="password" name="password" value="{{ old('password') }}" />
            @if ($errors->has('password'))
              <p class="form-hint">{{ $errors->first('password') }}</p>
            @endif
          </div>

          <div class="form-field {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            <label for="password_confirmation" class="label">Repita a senha</label>
            <input type="password" class="text-field" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" />
            @if ($errors->has('password_confirmation'))
              <p class="form-hint">{{ $errors->first('password_confirmation') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="flex flex-col md:flex-row py-4">
        <div class="px-2 w-full md:w-2/5 xl:w-1/4 text-center md:text-left">
          <h2 class="text-xl font-bold mb-2">Regulamento</h2>
        </div>

        <div class="w-full md:w-3/5 xl:w-1/2 mt-4 md:mt-0">
          <div class="form-field {{ $errors->has('agreed') ? 'has-error' : '' }}">
            <div class="checkbox-container">
              <input type="checkbox" id="agreed" value="true" name="agreed" {{ (bool)old('agreed') ? 'checked' : '' }} />
              <label for="agreed" class="label">Li e concordo com o <a href="/downloads/regulamento.pdf" target="_blank">Regulamento do Evento</a></label>
            </div>
            @if ($errors->has('agreed'))
              <p class="form-hint">{{ $errors->first('agreed') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="py-4 text-right">
        <button class="btn-primary" type="submit">Cadastrar</button>
      </div>
    </form>
  </div>
</div>
@endsection
