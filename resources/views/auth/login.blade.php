@extends('layouts.login')

@section('content')
  <div class="w-full max-w-sm" style="margin-top: 6rem">
    <h1 class="font-thin text-center mb-4">Faça login para continuar</h1>

    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('login') }}">
      @csrf

      <div class="mb-4 form-field {{ $errors->has('email') ? 'has-error' : '' }}">
        <label class="label capitalize" for="email">{{ __('validation.attributes.email') }}</label>
        <input name="email" class="text-field" id="email" type="text" placeholder="E-Mail" />
        @if ($errors->has('email'))
          <p class="form-hint">{{ $errors->first('email') }}</p>
        @endif
      </div>

      <div class="mb-6 form-field {{ $errors->has('password') ? 'has-error' : '' }}">
        <label class="label capitalize" for="password">{{ __('validation.attributes.password') }}</label>
        <input
          name="password"
          class="password-field"
          id="password"
          type="password"
          placeholder="******************"
        />
        @if ($errors->has('password'))
          <p class="form-hint">{{ $errors->first('password') }}</p>
        @endif
      </div>

      <div class="flex items-center justify-between">
        <button class="btn-primary" type="submit">{{ __('Login') }}</button>
        <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker" href="/">
          Voltar
        </a>
      </div>
    </form>
  </div>
@endsection
