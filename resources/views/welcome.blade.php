<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ADIRC Mistérios do Egito</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" />
        <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="home-page">
      <ul class="slideshow">
        <li><span>&nbsp;</span></li>
        <li><span>&nbsp;</span></li>
        <li><span>&nbsp;</span></li>
        <li><span>&nbsp;</span></li>
        <li><span>&nbsp;</span></li>
        <li><span>&nbsp;</span></li>
      </ul>

      <main class="main-content p-2">
        <h1 class="text-4xl font-thin text-center">Prepare-se para desvendar os mistérios do Egito</h1>

        <div class="mt-4 flex flex-col sm:flex-row">
          <a href="/register" class="text-center block no-underline w-64 my-4 sm:my-0 sm:mx-4 p-4 border-2 border-white font-bold text-2xl text-white rounded-lg hover:bg-white hover:text-black">Inscreva-se</a>
          <a href="/login" class="text-center block no-underline w-64 my-4 p-4 sm:my-0 sm:mx-4 border-2 border-white font-bold text-2xl text-white rounded-lg hover:bg-white hover:text-black">Entre</a>
        </div>

        <div class="mt-4 text-lg text-center">
          <a href="/list" target="_blank" class="text-white">
            Ainda está em dúvida? <br /> Confira aqui quem já está preparando sua caravana para o Egito
          </a>
        </div>
      </main>
    </body>
</html>
