<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="min-h-full">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>ADIRC Mistérios do Egito</title>

      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" />
      <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
  </head>
  <body class="user-list min-h-full">

    <div class="container mx-auto mt-8">
      <h1 class="font-thin text-4xl mt-4 mb-8">Confira a lista de inscritos para o evento</h1>

      <div>
        <table class="w-full text-left">
          <tr class="text-2xl">
            <th class="pb-4 border-b border-white">Nome</th>
            <th class="pb-4 border-b border-white">Clube</th>
          </tr>

          @foreach ($users as $user)
          <tr>
            <td class="py-4">{{ $user->name }}</td>
            <td class="py-4">{{ $user->club->name }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </body>
</html>
