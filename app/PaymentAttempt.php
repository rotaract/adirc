<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PaymentAttempt extends Model
{
    CONST IN_PROCESS = 'in_process';
    CONST APPROVED = 'approved';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function priceWithTax()
    {
        return $this->value + $this->tax;
    }

    public static function findForUser(User $user)
    {
        $attempt = self::firstOrCreate([
            'user_id' => $user->id,
            'ticket_batch' => config('payment.current_batch'),
            'value' => config('payment.current_batch_price'),
            'tax' => config('payment.current_batch_tax'),
        ]);

        if ($attempt->status === self::IN_PROCESS || $attempt->status === self::APPROVED) {
            return $attempt;
        }

        return tap($attempt, function ($attempt) {
            $attempt->ref = str_random(10);
            $attempt->save();
        });
    }
}
