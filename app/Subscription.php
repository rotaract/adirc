<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Subscription extends Model
{
    const NOT_PAID = 0;
    const PAID = 1;

    const PAYMENT_CREDIT_CARD = 1;
    const PAYMENT_DEPOSIT = 2;
    const PAYMENT_MONEY = 3;

    const TEXT_STATUS = [
        self::NOT_PAID => 'Não confirmada',
        self::PAID => 'Confirmada'
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function scopePaid($query)
    {
        return $query->where('status', self::PAID);
    }

    public static function paidByBatch()
    {
        return self::select(\DB::raw('count(id) as total,ticket_batch'))
                    ->paid()
                    ->groupBy('ticket_batch')->get()->keyBy('ticket_batch');
    }

    public function confirm($payment_type, $value, $tax, $ticket_batch)
    {
        $this->update([
            'payment_type' => $payment_type,
            'value' => $value,
            'tax' => $tax,
            'ticket_batch' => $ticket_batch,
            'status' => self::PAID,
        ]);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function paymentTypeText()
    {
        $types = [
            1 => 'Cartão de Crédito',
            2 => 'Depósito/Transferência',
            3 => 'Dinheiro',
        ];

        return $types[$this->payment_type];
    }
}
