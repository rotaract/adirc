<?php

namespace App;

use App\Mail\UserRegistered;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    const TYPE_MEMBER = 0;
    const TYPE_ADMIN = 1;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'birth_date' => 'datetime',
    ];

    public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public static function register($data)
    {
        $user = null;

        DB::transaction(function () use ($data, &$user) {
            $user = User::create($data);
            Subscription::create(['user_id' => $user->id]);
            Mail::to($user)->send(new UserRegistered($user));
        });

        return $user;
    }

    public function isAdmin()
    {
        return $this->type === self::TYPE_ADMIN;
    }

    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }
}
