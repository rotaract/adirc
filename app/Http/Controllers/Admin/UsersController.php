<?php

namespace App\Http\Controllers\Admin;

use App\Club;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::ofType(User::TYPE_MEMBER);

        if ($request->filled('club_id')) {
            $users->where('club_id', $request->club_id);
        }

        if ($request->filled('name')) {
            $users->where('name', 'ilike', "{$request->name}%");
        }

        if ($request->filled('status')) {
            $users->whereHas('subscription', function ($query) use ($request) {
                $query->where('status', $request->status);
            });
        }

        $users = $users->paginate(15);
        $clubs = Club::all();

        return view('admin.users.index', compact('users', 'clubs'));
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function payment(User $user)
    {
        return view('admin.users.payment', compact('user'));
    }

    public function updatePayment(User $user, Request $request)
    {
        $data = $request->validate([
            'payment_type' => ['required', Rule::in(['1', '2', '3'])],
            'ticket_batch' => 'required',
            'value' => 'required|numeric'
        ]);

        $user->subscription->update($data + ['status' => Subscription::PAID]);

        return redirect()->to("/admin/users/{$user->id}");
    }
}
