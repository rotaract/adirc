<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Subscription;
use App\User;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function names()
    {
        $users = User::with('club')->whereHas('subscription', function ($query) {
            $query->where('status', Subscription::PAID);
        })->orderBy('club_id')->get();

        return view('admin.reports.names', compact('users'));
    }

    public function allergies()
    {
        $users = User::with('club')
            ->whereHas('subscription', function ($query) {
                $query->where('status', Subscription::PAID);
            })
            ->orderBy('club_id')
            ->whereRaw("coalesce(allergy, '') <> ''")
            ->get();

        return view('admin.reports.allergies', compact('users'));
    }

    public function foodRestrictions()
    {
        $users = User::with('club')
            ->whereHas('subscription', function ($query) {
                $query->where('status', Subscription::PAID);
            })
            ->orderBy('club_id')
            ->whereRaw("coalesce(food_restriction, '') <> ''")
            ->get();

        return view('admin.reports.food_restrictions', compact('users'));
    }


}
