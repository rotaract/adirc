<?php

namespace App\Http\Controllers\Admin;

use App\Club;
use App\Http\Controllers\Controller;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function general()
    {
        $total = Subscription::paid()->count();
        $by_batch = Subscription::paidByBatch();
        $clubs = Club::orderBy('name', 'asc')->get();

        return view('admin.statistics.general', compact('total', 'by_batch', 'clubs'));
    }
}
