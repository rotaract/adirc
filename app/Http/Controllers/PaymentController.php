<?php

namespace App\Http\Controllers;

use App\PaymentAttempt;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        SDK::setClientId(config('payment.client_id'));
        SDK::setClientSecret(config('payment.client_secret'));
    }

    public function update()
    {
        $payment = Payment::find_by_id($_GET["id"]);
        if (!$payment) return '200 OK';

        $attempt = PaymentAttempt::where("ref", $payment->external_reference)->first();
        if (!$attempt) return '200 OK';

        $attempt->status = $payment->status;
        $attempt->payd_at = $payment->date_approved;
        $attempt->save();

        if ($attempt->status === 'approved') {
            $this->confirmSubscription($attempt);
        }

        return '200 OK';
    }

    public function confirmSubscription($attempt)
    {
        $subscription = Subscription::where('user_id', $attempt->user_id)->firstOrFail();

        $subscription->confirm(Subscription::PAYMENT_CREDIT_CARD, $attempt->value, $attempt->tax, $attempt->ticket_batch);
    }
}
