<?php

namespace App\Http\Controllers\Auth;

use App\Club;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['is_guest'] = isset($data['is_guest']) ? true : false;

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'birth_date' => ['required', 'date_format:Y-m-d', 'before:2001-04-14'],
            'phone' => ['required'],
            'club_id' => ['required', 'numeric', 'exists:clubs,id'],
            'address' => ['required'],
            'city' => ['required'],
            'is_guest' => ['required', 'boolean'],
            'blood_type' => ['required'],
            'emergency_contact_name' => ['required'],
            'emergency_contact_phone' => ['required'],
            'rg' => ['required'],
            'cpf' => ['required', 'cpf'],
            'agreed' => ['required', 'accepted'],
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $clubs = Club::orderBy('name', 'asc')->get();

        return view('auth.register', ['clubs' => $clubs]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @return \App\User
     */
    protected function create()
    {
        $data = request()->except('password_confirmation');
        $data['is_guest'] = isset($data['is_guest']) ? true : false;

        return User::register($data);
    }
}
