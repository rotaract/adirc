<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function userList()
    {
        $users = User::ofType(User::TYPE_MEMBER)->orderBy('name', 'asc')->get();

        return view('users', compact('users'));
    }
}
