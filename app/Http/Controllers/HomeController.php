<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\PaymentAttempt;
use Illuminate\Support\Facades\Auth;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        SDK::setClientId(config('payment.client_id'));
        SDK::setClientSecret(config('payment.client_secret'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attempt = PaymentAttempt::findForUser(auth()->user());
        $preference = $this->createPaymentPreference($attempt);

        $remaining_days = Carbon::now()->diffInDays(Carbon::parse(config('general.event_date')));

        return view('home', compact('remaining_days', 'preference'));
    }

    public function createPaymentPreference($payment_attempt)
    {
        $preference = new Preference();
        $preference->payment_methods = ["excluded_payment_types" => [["id" => "ticket"]]];
        $preference->external_reference = $payment_attempt->ref;

        $item = new Item();
        $item->title = config('payment.current_batch_name');
        $item->quantity = 1;
        $item->currency_id = "BRL";
        $item->unit_price = $payment_attempt->priceWithTax();

        $payer = new Payer();
        $payer->email = $payment_attempt->user->email;

        $preference->items = [$item];
        $preference->payer = $payer;

        $preference->save();

        return $preference;
    }
}
